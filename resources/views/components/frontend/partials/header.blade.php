<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">Ecommerce-B8</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Categories
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach($categories as $category)
                            <li><a class="dropdown-item" href="{{ route('frontend.products.index', $category->id) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="nav-item d-flex">
                        @auth
                        <a class="nav-link" href="{{ url('/cart') }}">
                            Cart [<span id="cartItemCount">{{ $cartItemCount }}</span>]
                        </a>
                        @endauth
                    </li>

                    <li class="nav-item d-flex">
                        @guest
                        <a class="nav-link" href="{{ url('/login') }}">
                            Login
                        </a>
                        <a class="nav-link" href="{{ url('/register') }}">
                            Register
                        </a>
                        @else
                        <a class="nav-link" href="{{ route('home') }}">
                            Dashboard
                        </a>
                        @endguest
                    </li>
                </ul>
                <form class="d-flex" action="{{ url('/') }}">
                    <input name="keyword" class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
</header>